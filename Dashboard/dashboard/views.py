from django.shortcuts import get_object_or_404, render
from django.contrib import auth
from django.contrib import messages
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

def welcome(request):
    context = {"welcome_page": "active"}
    return render(request,"welcome.html")

def admin_login(request):
    context = {"admin_login": "active"}
    return render(request,"admin_login.html")

def postlogin(request):
    userid = request.POST.get("userid")
    password = request.POST.get("password")
    if (userid=="admin" and password=="rioadm"):
        return render(request,"adm_home.html",{'loggeduser':userid})
    else:
        messages.warning(request, messages.INFO, 'Invalid Credentials! Please try again.')
        return render(request,"admin_login.html")
    return render(request,"admin_login.html",{'loggeduser':userid})

def adm_home(request):
    context = {"adm_home": "active"}
    return render(request,"adm_home.html")

def flight_details(request):
    context = {"flight_details": "active"}
    return render(request,"flight_number.html")

def passenger_home(request):
    context = {"passenger_home": "active"}
    return render(request,"passenger_home.html")

def airport_spa(request):
    context = {"airport_spa": "active"}
    return render(request, "airport_spa.html")

def airport_lounge(request):
    context = {"airport_lounge": "active"}
    return render(request, "airport_lounge.html")

def airport_coffee(request):
    context = {"airport_coffee": "active"}
    return render(request, "airport_coffee.html")

def airport_resta(request):
    context = {"airport_resta": "active"}
    return render(request, "airport_resta.html")

def airport_fast(request):
    context = {"airport_fast": "active"}
    return render(request, "airport_fast.html")

def airport_food(request):
    context = {"airport_food": "active"}
    return render(request, "airport_food_all.html")

def airport_sight(request):
    context = {"airport_sight": "active"}
    return render(request, "airport_sightseeing.html")

def airport_salon(request):
    context = {"airport_salon": "active"}
    return render(request, "airport_salon.html")

def airport_shopping(request):
    context = {"airport_shopping": "active"}
    return render(request, "airport_shopping.html")

def airport_beauty(request):
    context = {"airport_beauty": "active"}
    return render(request, "airport_beauty.html")

def airport_books(request):
    context = {"airport_books": "active"}
    return render(request, "airport_books.html")

def airport_malls(request):
    context = {"airport_malls": "active"}
    return render(request, "airport_malls.html")

def airport_pharmacy(request):
    context = {"airport_pharmacy": "active"}
    return render(request, "airport_pharmacy.html")

def airport_fashion(request):
    context = {"airport_fashion": "active"}
    return render(request, "airport_fashion.html")

def rio_hotels(request):
    context = {"rio_hotels": "active"}
    return render(request, "rio_hotels.html")

def rio_hotel_info(request):
    context = {"rio_hotel_info": "active"}
    hotel_id = request.GET.get('hotelID')
    df = pd.read_csv('static/data/Rio_Hotels_Clean.csv')
    hotel_name = df.loc[int(hotel_id)]['HotelName']
    df.set_index('HotelName', inplace = True)
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df=0, stop_words='english')
    tfidf_matrix = tf.fit_transform(df['Description_Clean'])
    cosine_similarities = cosine_similarity(tfidf_matrix, tfidf_matrix)
    indices = pd.Series(df.index)
    recommended_hotels = []
    idx = indices[indices == hotel_name].index[0]
    score_series = pd.Series(cosine_similarities[idx]).sort_values(ascending = False)
    top_10_indexes = list(score_series.iloc[1:11].index)
    for i in top_10_indexes:
        recommended_hotels.append(list(df.index)[i])
    return render(request, "rio_hotel_info.html", {'recommendations': json.dumps(recommended_hotels)})
