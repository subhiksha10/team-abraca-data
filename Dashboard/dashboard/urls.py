"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$',views.welcome,name="welcome"),
    url(r'^login/',views.admin_login,name="login"),
    url(r'^postlogin/',views.postlogin,name="postlogin"),
    url(r'^admin_home/',views.adm_home,name="adm_home"),
    url(r'^flight_details/',views.flight_details,name="flight_details"),
    url(r'^passenger_home/',views.passenger_home,name="passenger_home"),
    url(r'^airport_spa/',views.airport_spa,name="airport_spa"),
    url(r'^airport_lounge/',views.airport_lounge,name="airport_lounge"),
    url(r'^airport_coffee/',views.airport_coffee,name="airport_coffee"),
    url(r'^airport_resta/',views.airport_resta,name="airport_resta"),
    url(r'^airport_fast/',views.airport_fast,name="airport_fast"),
    url(r'^airport_food/',views.airport_food,name="airport_food"),
    url(r'^airport_sightseeing/',views.airport_sight,name="airport_sight"),
    url(r'^airport_salon/',views.airport_salon,name="airport_salon"),
    url(r'^airport_shopping/',views.airport_shopping,name="airport_shopping"),
    url(r'^airport_beauty/',views.airport_beauty,name="airport_beauty"),
    url(r'^airport_books/',views.airport_books,name="airport_books"),
    url(r'^airport_malls/',views.airport_malls,name="airport_malls"),
    url(r'^airport_pharmacy/',views.airport_pharmacy,name="airport_pharmacy"),
    url(r'^airport_fashion/',views.airport_fashion,name="airport_fashion"),
    url(r'^rio_hotels$',views.rio_hotels,name="rio_hotels"),
    url(r'^rio_hotel_info$',views.rio_hotel_info, name="rio_hotel_info" ),
]
